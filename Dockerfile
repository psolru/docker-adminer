ARG ADMINER_VERSION=latest
FROM adminer:${ADMINER_VERSION}

COPY index.php /var/www/html/index.php
