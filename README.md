# Dockerized Adminer with autologin

[![Docker Pulls](https://img.shields.io/docker/pulls/psolru/docker-adminer)](https://hub.docker.com/r/psolru/docker-adminer)
[!["Buy Me A Coffee"](https://www.buymeacoffee.com/assets/img/custom_images/orange_img.png)](https://www.buymeacoffee.com/psolru)

Dockerized [Adminer](https://www.adminer.org/), which provides the ability to automatically log in / pre-populate credentials via ENV variables.

## Environment variables
All environment variables are optional! The login screen will be empty if you do not specify them.

| Variablename        | Description                                    | Example                                                                                                                       |
| ------------------- | ---------------------------------------------- | ----------------------------------------------------------------------------------------------------------------------------- |
| ADMINER_DRIVER      | (string) Database type your target server runs | *`sqlite`, `sqlite2`, `pgsql`, `oracle`, `mssql`, `firebird`, `simpledb`, `mongo`, `elastic`, `clickhouse` (empty for mysql)  |
| ADMINER_SERVER      | (string) Database server address               | localhost:3307 / localhost / mysql                                                                                            |
| ADMINER_USERNAME    | (string) Specify user                          | root                                                                                                                          |
| ADMINER_PASSWORD    | (string) Password of specified user            | changeme                                                                                                                      |
| ADMINER_DB          | (string) Database to login to                  | default                                                                                                                       |
| **ADMINER_AUTOLOGIN | (bool) Autologin user with given credentials   | 0                                                                                                                             |

*This image is not tested with all these drivers, some are also marked as beta/alpha by Adminer.  
**Important. Make sure that you specify the correct credentials when using `ADMINER_AUTOLOGIN`, otherwise you will get an infinite loop...

## Examples

Docker Compose
```yml
---
version: "3.0"
services:
  web:
    image: psolru/docker-adminer
    ports:
      - "8080:8080"
    environment:
      - ADMINER_AUTOLOGIN=0
      - ADMINER_DB=default
      - ADMINER_PASSWORD=changeme
      - ADMINER_SERVER=mysql
      - ADMINER_USERNAME=root
  mysql:
    image: mysql:8.0.3
    environment:
      - MYSQL_ROOT_PASSWORD=changeme
      - MYSQL_DATABASE=default
```
