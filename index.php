<?php
function adminer_object() {
    return new class extends Adminer {
        public function loginForm() {
            parent::loginForm();

            if ($this->getEnv('ADMINER_AUTOLOGIN')) {
                echo script('
                    document.addEventListener(\'DOMContentLoaded\', function () {
                        document.forms[0].submit()
                    })
                ');
            }
        }

        public function loginFormField($name, $heading, $value) {
            $envValue = $this->getAutoLoginCredentials($name);

            if ($envValue !== null) {
                $value = sprintf(
                    '<input name="auth[%s]" type="%s" value="%s">',
                    h($name),
                    h($name === 'password' ? 'password' : 'text'),
                    h($envValue)
                );
            }

            return parent::loginFormField($name, $heading, $value);
        }

        public function getAutoLoginCredentials($key) {
            switch ($key) {
                case 'db':       return $this->getEnv('ADMINER_DB');
                case 'driver':   return $this->getEnv('ADMINER_DRIVER');
                case 'password': return $this->getEnv('ADMINER_PASSWORD');
                case 'server':   return $this->getEnv('ADMINER_SERVER');
                case 'username': return $this->getEnv('ADMINER_USERNAME');
                default:         return null;
            }
        }

        private function getEnv($key) {
            return getenv($key) ?: null;
        }
    };
}

require __DIR__ . '/adminer.php';
